from abc import ABC, abstractmethod

class Person(ABC):
	@abstractmethod
	def get_full_name():
		pass

	@abstractmethod
	def add_request():
		pass

	@abstractmethod
	def check_request():
		pass

	@abstractmethod
	def add_user():
		pass

class Employee(Person):
	def __init__(self, first_name, last_name, email, department):
		super().__init__()
		self._first_name = first_name
		self._last_name = last_name
		self._email = email
		self._department = department

	def set_first_name(self, first_name):
		self._first_name = first_name

	def set_last_name(self, last_name):
		self._last_name = last_name

	def set_email(self, email):
		self._email = email

	def set_department(self, department):
		self._department = department

	def get_first_name(self):
		print(f'First name: {self._first_name}')

	def get_last_name(self):
		print(f'Last name: {self._last_name}')

	def get_email(self):
		print(f'Email: {self._email}')

	def get_department(self):
		print(f'Department: {self._department}')

	def get_full_name(self):
		return f'{self._first_name} {self._last_name}'

	def add_request(self):
		return 'Request has been added'

	def check_request(self):
		return 'Request has been checked'

	def add_user(self):
		return 'Employee cannot add user'

	def login(self):
		return f'{self._email} has logged in'

	def logout(self):
		return f'{self._email} has logged out'

class TeamLead(Person):
	def __init__(self, first_name, last_name, email, department):
		super().__init__()
		self._first_name = first_name
		self._last_name = last_name
		self._email = email
		self._department = department
		self._member_list = []

	def set_first_name(self, first_name):
		self._first_name = first_name

	def set_last_name(self, last_name):
		self._last_name = last_name

	def set_email(self, email):
		self._email = email

	def set_department(self, department):
		self._department = department

	def get_first_name(self):
		print(f'First name: {self._first_name}')

	def get_last_name(self):
		print(f'Last name: {self._last_name}')

	def get_email(self):
		print(f'Email: {self._email}')

	def get_department(self):
		print(f'Department: {self._department}')

	def get_full_name(self):
		return f'{self._first_name} {self._last_name}'

	def add_request(self):
		return 'Request has been added'

	def check_request(self):
		return 'Request has been checked'

	def add_user(self):
		return 'User has been added'

	def login(self):
		return f'{self._email} has logged in'

	def logout(self):
		return f'{self._email} has logged out'

	def add_member(self, employee):
		self._member_list.append(employee)

	def get_members(self):
		return self._member_list

class Admin(Person):
	def __init__(self, first_name, last_name, email, department):
		super().__init__()
		self._first_name = first_name
		self._last_name = last_name
		self._email = email
		self._department = department

	def set_first_name(self, first_name):
		self._first_name = first_name

	def set_last_name(self, last_name):
		self._last_name = last_name

	def set_email(self, email):
		self._email = email

	def set_department(self, department):
		self._department = department

	def get_first_name(self):
		print(f'First name: {self._first_name}')

	def get_last_name(self):
		print(f'Last name: {self._last_name}')

	def get_email(self):
		print(f'Email: {self._email}')

	def get_department(self):
		print(f'Department: {self._department}')

	def get_full_name(self):
		return f'{self._first_name} {self._last_name}'

	def add_request(self):
		return 'Request has been added'

	def check_request(self):
		return 'Request has been checked'

	def add_user(self):
		return 'User has been added'

	def login(self):
		return f'{self._email} has logged in'

	def logout(self):
		return f'{self._email} has logged out'

class Request():
	def __init__(self, name, requester, date_requested, status = 'open'):
		self._name = name
		self._requester = requester
		self._date_requested = date_requested
		self._status = status

	def set_name(self, name):
		self._name = name

	def set_requester(self, requester):
		self._requester = requester

	def set_date_requested(self, date_requested):
		self._date_requested = date_requested

	def set_status(self, status):
		self._status = status

	def get_name(self):
		print(f'Name: {self._name}')

	def get_requester(self):
		print(f'Requester: {self._requester}')

	def get_date_requested(self):
		print(f'Date requested: {self._date_requested}')

	def get_status(self):
		print(f'Status: {self._status}')

	def update_request(self):
		return f'{self._name} has been updated'

	def close_request(self):
		return f'Request {self._name} has been closed'

	def cancel_request(self):
		return f'{self._name} has been cancelled'


employee1 = Employee("John", "Doe", "djohn@mail.com", "Marketing")
employee2 = Employee("Jane", "Smith", "sjane@mail.com", "Marketing")
employee3 = Employee("Robert", "Patterson", "probert@mail.com", "Sales")
employee4 = Employee("Brandon", "Smith", "sbrandon@mail.com", "Sales")
admin1 = Admin("Monika", "Justin", "jmonika@mail.com", "Marketing")
teamLead1 = TeamLead("Michael", "Specter", "smichael@mail.com", "Sales")
req1 = Request("New hire orientation", teamLead1, "27-Jul-2021")
req2 = Request("Laptop repair", employee1, "1-Jul-2021")

assert employee1.get_full_name() == "John Doe", "Full name should be John Doe"
assert admin1.get_full_name() == "Monika Justin", "Full name should be Monika Justin"
assert teamLead1.get_full_name() == "Michael Specter", "Full name should be Michael Specter"
assert employee2.login() == "sjane@mail.com has logged in"
assert employee2.add_request() == "Request has been added"
assert employee2.logout() == "sjane@mail.com has logged out"

teamLead1.add_member(employee3)
teamLead1.add_member(employee4)
for indiv_emp in teamLead1.get_members():
	print(indiv_emp.get_full_name())

assert admin1.add_user() == "User has been added"

req2.set_status("closed")
print(req2.close_request())